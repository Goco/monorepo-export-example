# Monorepo + turborepo + pnpm issue

This is an official starter Turborepo using the [basic](https://github.com/vercel/turbo/tree/main/examples/basic) template.\
The only things that were changed are:
1. added Material UI library into the `packages/ui` using the command:
```sh
pnpm install @mui/material @emotion/react @emotion/styled
```
2. re-exported the material-ui from the `packages/ui/index.tsx`

## Steps to reproduce the error
Install dependencies by running `pnpm i` in the root folder.
The main problem is with auto-suggestions of the material-ui components outside of the ui package.

### What works:
Open `packages/ui/Header.tsx` and try to wrap the `{text}` into the Typography.
When you start writing Typog -> the idea is suggesting Typography from "@mui/material".

This works correctly because the MUI library is a direct dependency for this package.

### What doesn't work
Open `apps/web/app/page.tsx` and try to add the same component Typography.
Idea is not able to suggest the import from the `ui` package.
But if you explicitly write that import: `import { Button, Header, Typography } from "ui";`
Then everything works alright and I am able to see the suggested attributes for the Typography.\

This doesn't work because the MUI library is an indirect dependency through the `ui` package.


## P.S.
I was not configuring the emotion settings with the SSR so you cannot run the project. But for my case it is not relevant.
I was also not configuring eslint, so it is possible that the idea will be throwing some eslint errors.