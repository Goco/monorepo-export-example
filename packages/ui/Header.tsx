import * as React from "react";
import {Box} from "@mui/material";

export const Header = ({ text }: { text: string }) => {
  return <Box>{text}</Box>;
  // return <Box><Typography>{text}</Typography></Box>;
};
